# node-web-app
Taken from: https://nodejs.org/en/docs/guides/nodejs-docker-webapp/

# Building
Edit package.json author line and run, replacing <your username> w/your username or just remove it:
```
docker build -t <your username>/node-web-app .
```

List your image:
```
docker images
```

# Running
```
docker run -p 49160:8080 -d <your username>/node-web-app
```

# Testing
Check for container running:
```
docker ps
```
Check for node server up:
```
curl -i localhost:49160
```

Should see in curl output:
```
Hello world
```
